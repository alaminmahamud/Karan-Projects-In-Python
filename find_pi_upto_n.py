#! /usr/bin/python3
"""
Name: Find PI upto Nth Digit
Purpose: Get the value of PI to n number of decimal places
Author:
Algorithm: Chudnovsky Algorithm
Date: 26.March.2018
License: 


Module Dependencies:
pass
"""
from __future__ import print_function
import sys, math
import decimal


decimal.getcontext().rounding = decimal.ROUND_FLOOR
sys.setrecursionlimit(100000)

is_python2 = sys.version_info[0] == 2
if is_python2:
    input = raw_input


def factorial(n):
    """
    Summary: returns the factorial


    Description:
    ------------
    this factorial is calculated using recursion technique.


    Paramters:
    ----------
    n -- Number to get factorial of


    Returns:
    --------
    returns The Factorial(n)
    """
    if not n:
        return 1
    return n * factorial(n - 1)


def getIteratedValue(k):
    """
    Summary: Return the iteration as given in the Chudnovsky algorithm.


    Description:
    ------------
    k iterations give k-1 decimal places..
    Since we need k decimal places
    make iteration to k+1.


    Paramters:
    ----------
    k -- Number of Decimal Digits to get.


    Returns:
    --------
    the Iterated value.
    """
    k = k+1
    decimal.getcontext().prec = k
    sum = 0
    for k in range(k):
        first = factorial(6*k) * (13591409 + 545140134*k)
        down = factorial(3*k) * (factorial(k))**3 * (640320**(3*k))

        sum += first/down
    return decimal.Decimal(sum)


def getValueOfPi(k):
    """
    Summary: returns the value of pi using Chudnovsky Algorithm


    Description:
    ------------
    PI = (426880*sqrt(10005))/(iteration)

    Paramters:
    ----------
    k = is the number of decimal places we need to calculate.


    Returns:
    --------
    Returns the value of PI upto Kth Digits.
    """
    iter = getIteratedValue(k)
    up = 426880 * math.sqrt(10005)
    pi = decimal.Decimal(up) / iter

    return pi


def shell():
    """
    Console Function to create the interactive shell.

    Parameters:
    ----------

    Returns:
    -------
    None
    """
    print(
        "Welcome to PI Calculator.",
        "In the shell below Enter the number of digits",
        " upto which the value should be calculated."
    )

    while True:
        print(">>> ", end='')
        entry = input()
        if entry == "quit()":
            break
        elif not entry.isdigit():
            print("You did not enter a number. Try again.")
        else:
            value = getValueOfPi(int(entry))
            print(value)
            print(len(str(value)))


if __name__ == "__main__":
    shell()
